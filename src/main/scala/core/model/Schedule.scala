package core.model

import java.time.LocalTime

import core.model.Day.WeekDay

import scala.collection.mutable

object Day extends Enumeration {
  type WeekDay = Value
  val Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday = Value

  val days = List(Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday)
}

case class Schedule(dailySchedules: Set[DailySchedule], course: Course, semester: Semester) {
  require(dailySchedules != null && dailySchedules.nonEmpty)
  require(course != null)
  require(semester != null)
}

case class Course(speciality: String, yearOfStudy: Int) {
  require(speciality != null && speciality.nonEmpty)
  require(yearOfStudy > 0 && yearOfStudy < 7)
}

case class Semester(number: Int) {
  require(number >= 0)
}

case class DailySchedule(day: WeekDay, lesson: Set[Lesson]) {
  require(day != null)
}

case class Duration(id: Int, startTime: LocalTime, endTime: LocalTime) {
  require(startTime.compareTo(endTime) < 0)
}

case class Lesson(day: WeekDay, course: Course, semester: Semester, duration: Duration,
                  name: LessonName, teacher: Teacher,
                  groupType: GroupType, weeks: Set[Week], auditory: Auditory) {
  require(day != null)
  require(course != null)
  require(semester != null)
  require(duration != null)
  require(name != null)
  require(teacher != null)
  require(weeks.nonEmpty)
  require(weeks.size <= 15)
}

case class LessonName(name: String) {
  require(name != null && name.nonEmpty)
}

case class Teacher(name: String, degree: String) {
  require(name != null && name.nonEmpty)
  require(degree != null && degree.nonEmpty)
}

object Week {
  private val cache = new mutable.HashMap[Int, Week]()

  def apply(week: Int): Week = cache.getOrElseUpdate(week, { new Week(week) })
}

case class Week(week: Int) {
  require(week >= 1 && week <= 15)
}

sealed class GroupType

case class Group(name: String) extends GroupType {
  require(name != null && name.nonEmpty)
}

object Lecture extends GroupType {
  override def toString: String = "LECTURE"
}

case class Auditory(building: Int, classNo: String) {
  require(building > 0)
  require(classNo != null && classNo.nonEmpty)
}