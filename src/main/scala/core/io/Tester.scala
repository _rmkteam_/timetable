package core.io
import java.io.{File, PrintWriter}
import java.time.LocalTime

import core.AuditoryParser
import core.model.Day.WeekDay
import core.model._
import net.liftweb.json.Extraction._
import net.liftweb.json.{DefaultFormats, _}

class AddressSerializer extends Serializer[WeekDay] {
  private val Class = classOf[WeekDay]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), WeekDay] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: WeekDay => JInt(x.id)
  }
}

class SemesterSerializer extends Serializer[Semester] {
  private val Class = classOf[Semester]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Semester] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: Semester => JInt(x.number)
  }
}

class WeekSerializer extends Serializer[Week] {
  private val Class = classOf[Week]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Week] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: Week => JInt(x.week)
  }
}

class LessonNameSerializer extends Serializer[LessonName] {
  private val Class = classOf[LessonName]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), LessonName] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: LessonName => JString(x.name)
  }
}

class TimeSerializer extends Serializer[LocalTime] {
  private val Class = classOf[LocalTime]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), LocalTime] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: LocalTime => JString(x.toString)
  }
}

class AuditorySerializer extends Serializer[Auditory] {
  private val Class = classOf[Auditory]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Auditory] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case x: Auditory => new JObject(List(JField("building", JString(x.building.toString)), JField("classNo", JString(x.classNo.toString))))
  }
}

class GroupSerializer extends Serializer[GroupType] {
  private val Class = classOf[WeekDay]

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), GroupType] = {
    case (TypeInfo(Class, _), json) => ???
  }

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case Lecture => JString(Lecture.toString)
    case x: Group => JString(x.name)
  }
}


object Tester extends App {

  val result = ScheduleParser(List(new File("src/input"))).parse()

 // println(result.iterator.next()) // if you don't supply your own Protocol (see below) // if you don't supply your own Protocol (see below)
  //println(new Gson().toJson(result.iterator.next()))

  implicit val formats = DefaultFormats + new AddressSerializer + new GroupSerializer + new TimeSerializer + new AuditorySerializer + new WeekSerializer + new LessonNameSerializer + new SemesterSerializer


  val json = prettyRender(decompose(result.map(s => s.dailySchedules).flatten.map(m => m.lesson).flatten))

  //new PrintWriter("data.json") { write(json); close() }

{
  val json =  prettyRender(decompose(AuditoryParser(List(new File("src/auditories"))).parse()))

  new PrintWriter("auditories.json") { write(json); close() }
}

}


