package core

import java.io.File

import org.apache.poi.ss.usermodel.{Cell, CellType}

import scala.collection.mutable
import scala.util.Try

package object io {

  implicit class RichCell(cell: Cell) {
    def stringValue(): String = cell.getCellTypeEnum match {
      case CellType.NUMERIC => cell.getNumericCellValue.toString
      case CellType.STRING => cell.getStringCellValue
      case CellType.BLANK => ""
    }
  }

  def autoClose[A <: AutoCloseable, B](resource: A)(code: A => B): Try[B] = {
    val tryResult = Try {
      code(resource)
    }
    resource.close()
    tryResult
  }

  def collectFiles(files: Iterable[File]): Set[File] = {

    def collectFiles(file: File, files: mutable.Set[File]): Set[File] = {
      if (file.isDirectory) {
        file.listFiles().foreach(f => {
          if (f.isFile) {
            files += f
          } else {
            files ++= collectFiles(f, files)
          }
        })
      } else {
        files += file
      }

      files.toSet
    }

    files flatMap (f => collectFiles(f, new mutable.HashSet[File]())) filter (f => !f.isHidden) toSet
  }
}
