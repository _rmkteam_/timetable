package core.io

import java.io.{File, FileInputStream}
import java.time.LocalTime
import java.time.format.DateTimeFormatter

import core.model.Day.WeekDay
import core.model._
import org.apache.poi.ss.usermodel.{Cell, CellType, Row}
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.util.Try

//import scala.collection.JavaConversions._

object ScheduleParser {
  def apply(inDirs: List[File]): ScheduleParser = new ScheduleParser(inDirs)
}

final class ScheduleParser(inDirs: List[File]) {

  private val semesters = List("осінній", "весняний", "додатковий")
  private val timeFormatter = DateTimeFormatter.ofPattern("H:mm")

  require(inDirs.forall(f => f.exists()), s"Files don't not exist ${inDirs.filter(f => !f.exists())}")

  def parse(): Set[Schedule] = collectFiles(inDirs) map parse filter (t => t isSuccess) map (t => t get)

  private case class LessonContext(day: WeekDay, course: Course, semester: Semester, duration: Duration)

  private def canParse(cell: Cell) = cell != null && (cell.getCellTypeEnum == CellType.STRING || cell.getCellTypeEnum == CellType.NUMERIC)

  private def parse(file: File): Try[Schedule] = {
    println(s"parsing file $file")

    autoClose(new XSSFWorkbook(new FileInputStream(file))) {
      workBook =>

        val sheet = workBook.getSheetAt(0)
        var duration: Duration = null
        var day: WeekDay = null
        val lessons = new mutable.LinkedHashSet[Lesson]()
        val dailySchedule = new mutable.LinkedHashSet[DailySchedule]()

        val heading = sheet.getRow(6).getCell(0).stringValue().trim
        val index = heading.indexOf(',')
        val course = Course(heading.substring(0, index), ("""\d+""".r findFirstIn heading).get.toInt)
        val semester = parseSemester(sheet.getRow(7).getCell(0).stringValue().trim, course.yearOfStudy)

        for {
          row <- sheet.rowIterator()
          if row.getRowNum > 9
        } {

          if (canParse(row.getCell(0))) {

            dailySchedule ++= Option(day).map(day => DailySchedule(day, lessons.toSet))

            day = parseDayOfWeek(row.getCell(0).stringValue())
            lessons.clear()
          }

          if (canParse(row.getCell(1))) {
            val split = row.getCell(1).getStringCellValue.split('-')

            val start = LocalTime.parse(split(0).replace('.', ':'), timeFormatter)
            val end = LocalTime.parse(split(1).replace('.', ':'), timeFormatter)

            duration = Duration(toLessonId(start), start, end)
          }

          lessons ++= Option(row.getCell(2))
            .flatMap(_ => parseLesson(row, LessonContext(day, course, semester, duration)))
        }

        Schedule(dailySchedule.toSet, course, semester)
    }
  }

  private def parseLesson(row: Row, context: LessonContext): Option[Lesson] = {
    def parseWithShift(shift: Int): Option[Lesson] = {
      val title = row
        .getCell(2 + shift)
        .stringValue()
        .trim

      if (title.nonEmpty) {

        val value = row.getCell(3 + shift).stringValue().trim
        val spaceIndex = value.indexOf(' ')

        val lessonName = LessonName(title)

        val lesson = Lesson(
          context.day,
          context.course,
          context.semester,
          context.duration,
          lessonName,
          Teacher(value.substring(0, spaceIndex), value.substring(spaceIndex)),
          parseLessonType(row.getCell(4 + shift)),
          parseWeeks(row.getCell(5 + shift).stringValue()),
          if (lessonName.name.toLowerCase.contains("фізичне виховання")) Auditory(3, "спортзал") else parseAuditory(row.getCell(6 + shift).getStringCellValue)
        )

        Some(lesson)
      } else {
        None
      }
    }

    // check if misplaced column, add shift to parse if needed
    val shift = if (row.getCell(2).getCellTypeEnum == CellType.NUMERIC) 1 else 0

    parseWithShift(shift)
  }

  private def parseSemester(in: String, yearOfStudy: Int): Semester = {
    val index = semesters.indexWhere(s => in.contains(s)) + 1

    require(index > 0, s"Not found semester for input $in")

    Semester(yearOfStudy * 3 + index)
  }

  private def toLessonId(start: LocalTime) = start.getHour match {
    case 8 => 1
    case 10 => 2
    case 11 => 3
    case 13 => 4
    case 15 => 5
    case 16 => 6
    case 18 => 7
  }

  private def parseDayOfWeek(string: String) = string.toLowerCase match {
    case "понеділок" => Day.Monday
    case "вівторок" => Day.Tuesday
    case "середа" => Day.Wednesday
    case "четвер" => Day.Thursday
    case "п`ятниця" | "п'ятниця" => Day.Friday
    case "субота" => Day.Saturday
    // what if =)
    case "неділя" => Day.Sunday
  }

  private def parseAuditory(string: String): Auditory = {
    val split = string.split('-').map(f => f.trim)

    Auditory(split(0).toInt, split(1))
  }

  private def parseLessonType(cell: Cell) = cell.getCellTypeEnum match {
    case CellType.STRING if cell.stringValue().toLowerCase == "лекція" => Lecture
    case CellType.STRING => Group(cell.stringValue())
    case _ => Group(cell.getNumericCellValue.toInt.toString)
  }

  private def parseWeeks(string: String): Set[Week] = string.split(',')
    .foldLeft(mutable.HashSet[Int]()) { (acc, curr) =>
      val sp = curr.split('-')

      sp.length match {
        case 1 => acc += sp(0).trim.toInt
        case 2 => acc ++= sp(0).trim.toInt to sp(1).trim.toInt
      }
      acc
    }
    .map(i => Week(i))
    .toSet

}