package core

import java.io.{File, FileInputStream}

import core.io._
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import scala.collection.JavaConversions._

case class AuditoryInfo(name: String, places: Int, hasProjector: Boolean, isComputerClass: Boolean, hasBoard: Boolean)

object AuditoryParser {
  def apply(inDirs: List[File]): AuditoryParser = new AuditoryParser(inDirs)
}

class AuditoryParser(inDirs: List[File]) {


  def parse(): Set[AuditoryInfo] = core.io.collectFiles(inDirs).map(parse).filter(t => t.isSuccess).flatMap(t => t.get)

  private def parse(file: File) = {
    autoClose(new XSSFWorkbook(new FileInputStream(file))) {
      workBook =>

        val sheet = workBook.getSheetAt(0)

        val result = for (
          row <- sheet.rowIterator()
          if row.getRowNum > 1 && row.getCell(0).getCellTypeEnum == CellType.STRING
        ) yield {
          try {
            AuditoryInfo(
              row.getCell(0).stringValue().trim,
              row.getCell(1).stringValue().trim.split('-')(0).toDouble.toInt,
              toBoolean(row.getCell(2).stringValue().trim),
              toBoolean(row.getCell(3).stringValue().trim),
              toBoolean(row.getCell(4).stringValue().trim)
            )
          } catch {
            case e: Throwable => e.printStackTrace()
              throw e
          }

        }

        result.toList
    }
  }

  private def toBoolean(input: String) = input.toLowerCase match {
    case "так" => true
    case _ => false
  }

}
